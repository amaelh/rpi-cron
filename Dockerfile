# Pull base image
FROM alpine:3.15

# Add bash and docker, to be able to run commands in other containers
RUN apk add --no-cache bash docker

# Add file
COPY healthcheck.sh /

HEALTHCHECK CMD /healthcheck.sh --retries=0 --start-period=1s

# Define default command
CMD ["/usr/sbin/crond", "-f", "-d", "8"]
