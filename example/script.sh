#!/bin/bash

echo "Start"

echo "Test stdout on $(date)"
echo "Test stderr on $(date)" >&2

vl_randomContainer=$(docker ps -a | tail -1 | cut -d' ' -f 1) 
docker exec ${vl_randomContainer} sh -c "echo \"Hello from inside another container on $(date)\""

echo "End"
