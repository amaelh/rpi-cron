# rpi-cron
Simple CRON image including docker to be able ton run commands in other containers

## General Usage
Minimal alpine raspberry pi image to schedule scripts, including bash for convenience and docker to be able to run commands in other containers.

Requirements :  mount a volume on /etc/cron.d
- Each cron file **has to be owned by root:root** with no write access to group nor others
- Each script has to be executable (a+x)

## Examples
See examples folder for examples of docker-compose file, crontab and scripts showing how to handle stdout, stderr, and docker calls.

