#!/bin/sh

#
# Crontab image health check
#

# Check crontab files owners and permissions
if [[ `find /etc/crontabs/  \( -perm -g=w -o -perm -o=w -o ! -user root \) | wc -l` -ne 0 ]]
then
	echo "Bad owner or permissions in /etc/crontabs/ (must be owned by root and chmod go-w)"
	return 1
fi

# Count processes
if [[ `ps -ef | grep /usr/sbin/crond | grep  -v grep | wc -l` -ne 1 ]]
then
	echo "Not exactly 1 crond process not found"
	return 1
fi

# Else ervything is healthy
return 0
